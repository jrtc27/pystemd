Source: pystemd
Section: python
Priority: optional
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders: Alexandros Afentoulis <alexaf.dpkg@bloom.re>
Build-Depends: debhelper-compat (= 12),
               dh-python,
               libsystemd-dev,
               python3-all-dev,
               python3-mock,
               python3-setuptools,
               python3-six
Standards-Version: 4.4.1
Homepage: https://github.com/facebookincubator/pystemd
Vcs-Browser: https://salsa.debian.org/python-team/modules/pystemd
Vcs-Git: https://salsa.debian.org/python-team/modules/pystemd.git

Package: python3-pystemd
Architecture: any
Depends: dbus, systemd, ${misc:Depends}, ${python3:Depends}, ${shlibs:Depends}
Recommends: ${python3:Recommends}
Suggests: ${python3:Suggests}
Description: systemd binding for Python
 This library allows you to talk to systemd over D-Bus from Python, without
 actually thinking that you are talking to systemd over D-Bus. This allows you
 to programmatically start/stop/restart/kill and verify services status from
 systemd point of view, avoiding executing `subprocess.Popen(['systemctl', ...`
 and then parsing the output to know the result.
